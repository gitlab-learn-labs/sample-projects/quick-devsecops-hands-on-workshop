FROM python:alpine3.10

RUN apk update
RUN apk add --no-cache gcc musl-dev sqlite-dev

COPY ./requirements_test.txt /app/requirements_test.txt

WORKDIR /app
RUN pip install -r requirements_test.txt
COPY . /app

ENTRYPOINT [ "python" ]
CMD [ "run.py" ]
